#!/bin/bash

./sslocal -b "$SOCKS5_IF:$SOCKS5_PORT" -s "$HTTPS_HOST:$HTTPS_PORT" -m aes-256-cfb -k "$PASS" --plugin ./v2ray-plugin_linux_amd64 --plugin-opts="tls=true;host=$HTTPS_HOST;mode=websocket;loglevel=debug;path=$HTTPS_PATH;mux=8"
