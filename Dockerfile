FROM alpine:latest


RUN mkdir /opt/shadowsocks 
WORKDIR /opt/shadowsocks 
RUN apk update && apk upgrade && apk add bash iproute2 ca-certificates
RUN wget -q -O shadowsocks.tar.xz  'https://github.com/shadowsocks/shadowsocks-rust/releases/download/v1.7.0/shadowsocks-v1.7.0-nightly.x86_64-unknown-linux-musl.tar.xz'
RUN tar -xf shadowsocks.tar.xz && rm shadowsocks.tar.xz
RUN wget -q -O v2ray-plugin_linux_amd64.tar.gz 'https://github.com/shadowsocks/v2ray-plugin/releases/download/v1.1.0/v2ray-plugin-linux-amd64-v1.1.0.tar.gz'
RUN tar -xf v2ray-plugin_linux_amd64.tar.gz && rm v2ray-plugin_linux_amd64.tar.gz

ADD ./server.sh ./
ADD ./client.sh ./
RUN chmod +x ./server.sh ./client.sh

CMD [ "bash", "-c", "./server.sh"]
