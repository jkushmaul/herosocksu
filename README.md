

# Herosocksu

I like the idea of using Tor, but it’s slow and most of the time I don’t care about that. So I needed to get myself a VPN for $5/month. Screw that, I’ll build my own on AWS free tier but those are only free for a period.  Heroku, on the other hand, has free dynos but the probem is, they only accept HTTPS and reverse proxy to HTTP service.

However, you can run docker containers - sky is the limit, with a few exceptions. (Docker in Docker is a no-go, for instance)

So started searching for a socks server I could run. I mean, Squid - right? What else could there be, but it was too much for my needs? I found a few but the best one sounded like [https://shadowsocks.org/en/index.html](https://shadowsocks.org/en/index.html)

I figured I could just throw it on heroku and forward the port there - after all, https is just a layer right? But we know better.  I don't even know if it would matter - that might have made it harder.  This is a common configuration, to offload the ssl and forward internally to the app servers.

So that poses a problem when the request comes into socks5 listener - they aren’t speaking the same language. So we need to find the rosetta, something that the client can accept local socks requests, transmits them over https to heroku, which then ssl offloads and sends an http request to the other end of this. The server that takes http, and converts it to socks5.

So shadowsocks has plugin architecture. And they have a v2ray-plugin, which supports doing this very concept - even if they don’t know it.

The most common use case is clearly, that you are running a shadowsocks server with the shadowsocks port open to the internet. The flow is such
app->shadowsocksclient->internet
internet->shadowserver->target

My use case is different. I want to do

    app->shadowclient->https-socks translator-> https -> http->http-socks translator->shadowserver->target.

Why? Because it’s harder than it should be, and it’s a free proxy that isn’t on a proxy list. This was rough as well to bust this one out. The documentation was lacking, and apparently, no one else is talking about this.

## Setup:

I had a virtual machine with debian. I created a docker image which installed this:

[https://github.com/shadowsocks/shadowsocks-rust](https://github.com/shadowsocks/shadowsocks-rust)

[https://github.com/shadowsocks/v2ray-plugin](https://github.com/shadowsocks/v2ray-plugin)



## Files

-   `Client.sh`
-   `Dockerfile`
-   `heroku.yml server.sh`

### client.sh

    #!/bin/bash
    ./sslocal -b "$SOCKS5_IF:$SOCKS5_PORT" -s "$HTTPS_HOST:$HTTPS_PORT" -m aes-256-cfb -k "$PASS" --plugin ./v2ray-plugin_linux_amd64 --plugin-opts="tls=true;host=$HTTPS_HOST;mode=websocket;loglevel=debug;path=$HTTPS_PATH;mux=8"

### server.sh

    #/bin/bash
    ./ssserver -m aes-256-cfb -k "$PASS" -s "${HTTP_IF}:${PORT}" --plugin ./v2ray-plugin_linux_amd64 --plugin-opts="server;mode=websocket;loglevel=debug;path=$HTTP_PATH;mux=8"

### Dockerfile

    FROM alpine:latest
    RUN mkdir /opt/shadowsocks
    WORKDIR /opt/shadowsocks

    RUN apk update && apk upgrade && apk add bash iproute2 ca-certificates

    RUN wget -q -O shadowsocks.tar.xz 'https://github.com/shadowsocks/shadowsocks-rust/releases/download/v1.7.0/shadowsocks-v1.7.0-nightly.x86_64-unknown-linux-musl.tar.xz'
    RUN tar -xf shadowsocks.tar.xz && rm shadowsocks.tar.xz

    RUN wget -q -O v2ray-plugin_linux_amd64.tar.gz 'https://github.com/shadowsocks/v2ray-plugin/releases/download/v1.1.0/v2ray-plugin-linux-amd64-v1.1.0.tar.gz'
    RUN tar -xf v2ray-plugin_linux_amd64.tar.gz && rm v2ray-plugin_linux_amd64.tar.gz

    ADD ./server.sh ./
    ADD ./client.sh ./
    RUN chmod +x ./server.sh ./client.sh

    CMD [ "bash", "-c", "./server.sh"]


### Heroku.yml

```
build:
  docker:
    web: Dockerfile
```

## Putting it together

Determine a password you want to use for socks, a path for the url, and get your heroku cli working, create the repo, login to heroku, push/pull, containers, etc.  You'll need to set the server
config vars seen in `server.sh`

### Server

The Dockerfile is going to start the server automatically. SSL rev-proxy is going to send HTTP requests to this port. The server expects a few parameters.

-   `HTTP_IF` - this likely going to be 0.0.0.0, or may not even work if 127.0.0.1.
-   `HTTP_PATH` - up to you
-   `PASS` - use the same password on the client.
-   `PORT` - this may be passed by orchestration

Take note of your public URL and use it to fill it’s HTTPS vars.

Use your password in place of the subshell as a config var "PASS" and let it pass through.

You can upload your changes:
`git push heroku master`

### Client

The client expects the following parameters:

-   `PASS` - the password from first step.
-   `HTTPS_HOST` - from the url in previous section.
-   `HTTPS_PORT` - from the url in previous section.
-   `HTTPS_PATH` - up to you
-   `HOST_IF`    - The host Ip that clients will connect to.
-   `SOCKS5_IF`  - It could be local, it could be a docker interface if host networking.
-   `SOCKS5_PORT`-


```
docker run -i -t -p $HOST_IF:$SOCKS5_PORT:$SOCKS5_PORT -e "SOCKS5_IF=0.0.0.0" -e "SOCKS5_PORT=1080" -e "HTTPS_HOST=mypublicplace.localdomain" -e "HTTPS_PORT=443" -e "HTTPS_PATH=/proxy" -e "PASS=$(dd if=/dev/urandom of=/dev/stdout bs=1 count=32 status=none)" $YOURIMAGE ./client.sh
```

Of course, use your own password in place of the subshell.

Assuming there were no errors along the way, time to test.

### Test

Just use curl:

```
curl --socks5 172.17.0.2:1080 https://google.com/
```

    <HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
    <TITLE>301 Moved</TITLE></HEAD><BODY>
    <H1>301 Moved</H1>
    The document has moved
    <A HREF="https://www.google.com/">here</A>.
    </BODY></HTML>

### Network
Besides getting heroku up and running, you need a VM environment.

-   `VNet0` - Create a VM network and give it a private subnet of 10.0.0.0/24
-   `VM-socks` - It is running the client.sh command above.
-   `VM-proxied` - The proxied machine.

This is pretty simple, all the work is on `VM-socks` setup:  
`VM-proxied:10.0.0.2` forwards ip traffic to `VM-socks:10.0.0.1`  
`VM-socks:10.0.0.1` redirects all through `VM-socks:127.0.0.1:1080`  
Which then gets sent through to socks server via https+websockets translation.

#### `VM-socks`
I used Kali as the image for this one, because it has many of the tools I needed pre-installed.
It's not the most secure of distros - that's not a knock on it at all.  It's easy to get things Setup
for POC in it, because that's exactly what it is designed for.  But it is not designed be a hardened
system.

1. Eth0 will be your "public" interface with a Nat VM dhcpd ip.
2. Add second interface eth1 on `VM-network`, eth1, 10.0.0.1/24, you can just use the gui.
3. Save your initial iptables: `iptables-save > iptables.init`

##### Run herosocksu on localhost:1080
```
docker run -i -t --network-host -e "SOCKS5_IF=127.0.0.1" -e "SOCKS5_PORT=1080" \
  -e "HTTPS_HOST=mypublicplace.localdomain" -e "HTTPS_PORT=443" -e "HTTPS_PATH=/proxy" \
  -e "PASS=$(dd if=/dev/urandom of=/dev/stdout bs=1 count=32 status=none)" \
   $YOURIMAGE  \
    ./client.sh`
```
Of course, use your own password in place of the subshell and your docker image.

#### Setup redsocks
1. redsocks.local_ip=0.0.0.0
2. redsocks.local_port=1235
3. redsocks.ip=127.0.0.1
4. redsocks.port=1080
5. create iptables rules

For ease of PoC, I am simply allowing DNS to be SNAT'd directly
```
#!/bin/bash
#just let dns straight through for now
iptables -t nat -A POSTROUTING -o eth0 -p tcp --dport=53  -j MASQUERADE
iptables -t nat -A POSTROUTING -o eth0 -p udp --dport=53  -j MASQUERADE
iptables -t nat -A PREROUTING -p tcp --dport=53 -j ACCEPT
iptables -t nat -A PREROUTING -p udp --dport=53 -j ACCEPT

# create the REDSOCKS target
iptables -t nat -N REDSOCKS

#anything that needs to be routed coming in on eth1, send to REDSOCKS chain
iptables -t nat -A PREROUTING -i eth1 -p tcp -j REDSOCKS

# redirect statement sends everything else to the redsocks
iptables -t nat -A REDSOCKS -p tcp -j REDIRECT --to-ports $REDSOCKS_TCP_PORT

```

#### `VM-proxied`
Pretty simple here, just make sure it's vm network adapter is on the VMnet, the same one as `VM-socks:eth1`.
Give it an IP of `10.0.0.2`, def gw of `10.0.0.1` and any public DNS, e.g.)  `8.8.8.8`.

## Remarks
We already went pretty far with this, but many more items to work on.
1. multi-user public key authentication on shadowserver
There is a multiuser shadowsocks plugin I believe.  It's probably
not where one would want it yet though, and is likely not using any kind of public key authentication.
2. Tighten client security
Kali was used for ease of poc, but whonix gateway would be a good candidate.  One would need to ensure it's
properly marked as a public proxy gateway rather than a tor gateway.
3. This server can run anywhere there is a https offloading reverse proxy, which you can also run docker containers.
