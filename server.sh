#!/bin/bash

[ "x$DEBUG" -ne "x" ] || set -x;

./ssserver -m aes-256-cfb -k "$PASS" -s "${HTTP_IF}:${PORT}" --plugin ./v2ray-plugin_linux_amd64  --plugin-opts="server;mode=websocket;loglevel=debug;path=$HTTP_PATH;mux=8"
